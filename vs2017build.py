import glob
import os
import re
import shutil
import sys
import subprocess
import json

# ------------------------------ General Helpers ----------------------------------------

# Invoke Shell Command
def RunCommand(command):
	print(command)
	result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, 
		universal_newlines=True)
	print(result.stdout)
	print(result.stderr)
	print("Return Code: {0}".format(result.returncode))
	return (result.returncode, result.stdout, result.stderr)
	
	
# Make or clean out directory
def MakeDirectory(dir):
	if os.path.isdir(dir):
		shutil.rmtree(dir)
	os.makedirs(dir)
	

# Return a list of all files matching <filePattern> in the directory tree headed by <dir>
def GetAllVersionFiles(dir, filePattern):
	globPattern='{0}/**/{1}'.format(dir, filePattern)
	return glob.glob(globPattern, recursive=True)
	

# ---------------------------------- Argument Parsing -----------------------------------	

		
# Get a boolean value from a string.
def ParseBool(key, str):
	if (str.upper() in ['TRUE', 'T', 'YES', 'Y', '1']):
		return True
	elif (str.upper() in ['FALSE', 'F', 'NO', 'N', '0']):
		return False
	else:
		raise ValueError('Invalid value ({0}) when expecting a boolean argument for {1}'
			.format(str, key))

		
# Parse out the command line arguments. We're looking for the following options:
# 	First argument should be the solution file to build, with a path relative to this 
#   script.  All arguments below are optional, with their defaults as listed.
#   If any arguments have spaces in them, be sure to use quotes (as in default for 
#   platform.) 
#   Put no spaces around the equals signs.
#	Keys are all case insensitive. Values are case sensitive, depending on the tool 
#   the arguments are passed to.

#	target=<target> 
#		Default: Clean,Rebuild
# 	config=<build configuration>
#		Default: Release
#	buildPlatform=<platform>
# 		Default: "Any CPU" 
#   buildNodeReuse=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
#		Should MSBuild reuse build nodes (for speed, with some potential for hangs 
#		in some circumstances)
#		Default: True
#	test=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
#		Run unit tests?
#		Default: false
#	testFramework=<Framework35|Framework40|Framework45>
#		Test framework. See https://msdn.microsoft.com/en-us/library/jj155796.aspx
#		Default: Framework45
#	testPlatform=<x86|x64|ARM>
#		Platform architecture for tests. 
#		See https://msdn.microsoft.com/en-us/library/jj155796.aspx
#		Default: x86
#	cover=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
# 		Run coverage analysis on unit tests? (Forces test to true)
#		Default: false
#	failOnBranchCoverageBelow=<percent>
#		Return a non-zero result code if the branch coverage is lower than the 
#		specified percentage
#		Only applies if cover=true
#		Default: 0
#	archiveCoverage=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
#		Archive coverage results?
#		Only applies if cover=true
#		Default: False
#	archiveBuildFrom=<build output directory>
#		If non-empty, build artifacts from the specified directory will be collected 
#		to an artifacts folder.
#		Default: <empty string>
# 	nugetFetch=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
#		Fetch packages from Nuget before build
#		Default: False
#	buildTag=<tag or branch name>
#		Expected to be supplied by the GitLab CI system, as %CI_BUILD_TAG%. This will 
#		be the branch or tag name which is being built. 
#		NOTE: If this is not supplied on the command line, the environment variables 
#		will be checked, so normally this does not need to be supplied.
#		Default: <empty string>
#	buildId=<build number>
#		The automatically generated build number as supplied by GitLab, as %CI_BUILD_ID%
#		NOTE: If this is not supplied on the command line, the environment variables will 
#		be checked, so normally this does not need to be supplied.
#	nugetProject=<project>
#		If supplied, the named project will be packaged via NuGet and stored in the 
#		Tensentric library.
#		Standard naming conventions are assumed, so the project must live in a directory 
#		of the same name, immediately under the directory which contains the solution file. 
#		There must be a .nuspec configuration file in that directory, named the same as 
#		the project. 
#		E.g., ./SolutionFoo/ProjectBar/ProjectBar.nuspec
#		Default: <empty string>
#	release=<true|false|yes|no|y|n|t|f|1|0> (case insensitive)
#		Is this an offical release build? If so, version numbers will be stamped into C# 
#		AssemblyInfo source files.
#		Default: False
#	Examples:
#		Build Release configration:
#			vs2017build.py ./SolutionDir/SolutionFile.sln
#		Update NuGet dependencies, build, run tests, and generate coverage: (tests assume 
#		debug builds)
#			vs2017build.py ./SolutionDir/SolutionFile.sln config=Debug cover=t nugetFetch=t
#		Build for Release (tagged build) and push to NuGet
#			vs2017build.py ./SolutionDir/SolutionFile.sln buildTag=v1.0.3 nugetProject=ProjectName

def ParseArguments(argv):
	# Default options
	args = dict(
		SOLUTION = '',
		TARGET = 'Clean,Rebuild', 
		CONFIG = 'Release', 
		BUILDPLATFORM = '"Any CPU"', 
		BUILDNODEREUSE = True,
		TESTPLATFORM = 'x86', 
		TESTFRAMEWORK = 'Framework45',
		TEST = False,
		COVER = False,
		FAILONBRANCHCOVERAGEBELOW = 0,
		ARCHIVEBUILDFROM = '',
		ARCHIVECOVERAGE = False,
		NUGETFETCH = False,
		BUILDTAG='',
		BUILDID='',
		NUGETPROJECT='',
		RELEASE=False)

	pattern = re.compile('(\S+)=(.*)')
	argv.pop(0)
	args['SOLUTION'] = argv.pop(0)
	for arg in argv:
		m = pattern.match(arg)
		if (m!=None):
			# We found a match. Look for special cases which need to be interpreted, 
			# all others just get stored as strings.
			key = m.group(1).upper()
			val = m.group(2)
			if ((key=='TEST') or (key=='COVER') or (key=='BUILDNODEREUSE') or 
				(key=='NUGETFETCH') or (key=='RELEASE')):
				val = ParseBool(key, val)
			elif (key=='FAILONBRANCHCOVERAGEBELOW'):
				val = float(val)
			args[key]=val
	if (args['COVER']):
		args['TEST'] = True
	# Solution path and file
	solutionPath, solutionFile = os.path.split(args['SOLUTION'])
	args['SOLUTIONPATH']=solutionPath
	args['SOLUTIONFILE']=solutionFile
	print("----- Arguments -----")
	for key in args.keys():
		print("{0}={1}".format(key,args[key]))
	return args


# ---------------------------------- Build Steps ----------------------------------------
	
	
# Invoke the Visual Studio 2017 MSBuild compiler with the supplied arguments.	
def Build(args):
	# Compiler
	msbuild = '"C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe"'
	msbuildCommonArgs = (
		'/consoleloggerparameters:ErrorsOnly /maxcpucount /nologo /verbosity:quiet /nr:false')

	# Format build command
	targetArg = "/t:{0}".format(args['TARGET'])
	configArg = "/p:Configuration={0}".format(args['CONFIG'])
	platformArg = "/p:Platform={0}".format(args['BUILDPLATFORM'])
	buildNodeReuseArg = "/nr:{0}".format(str(args['BUILDNODEREUSE']))
	solutionArg = args['SOLUTION']
	buildCommand = ' '.join([msbuild, msbuildCommonArgs, targetArg, configArg, platformArg, 
		buildNodeReuseArg, solutionArg])
	
	# Invoke build command
	print("----- Build -----")
	result = RunCommand(buildCommand)
	return result[0]

	
# Detemine if test coverage is too low
def CheckTestCoverage(args, stdout):
	if (args['COVER']):
		print()
		print("Checking coverage")
		limit = args['FAILONBRANCHCOVERAGEBELOW']
		coveragePattern = re.compile(r"Visited Branches.*\((.*)\)")
		match = coveragePattern.search(stdout)
		if (match != None):
			coverage = float(match[1])
			print("Covered branches = {0}. Minimum acceptable = {1}"
				.format(coverage, limit))
			if (coverage < limit):
				print("FAIL: Insufficient coverage.")
				return -1
			print("Coverage Ok")
			return 0
		print("ERROR: Could not locate coverage result.")
		return -1


# Run all tests in the specified solution file, and generate raw coverage results.
def RunTests(args):
	opencover='C:\\Programs\\OpenCover\\OpenCover.4.6.519\\OpenCover.Console.exe'
	vstest = 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\IDE\\CommonExtensions\\Microsoft\\TestWindow\\vstest.console.exe'
	testPlatform=args['TESTPLATFORM']
	testFramework=args['TESTFRAMEWORK']

	OpenCoverRegister='administrator'

	# Setup the OpenCover filters to include everything, other than specific excluded 
	# assemblies and namespaces.
	# Identify all DLLs containing unit tests.
	# By convention, we name our unit test DLLs *.Tests.DLL, so they are easy to find 
	# by filename.
	pattern = re.compile(r"bin\\Debug")
	testDlls = []
	rootDir = args['SOLUTIONPATH']
	for root, subFolders, files in os.walk(rootDir):
		for file in files:
			pathAndFile = os.path.join(root,file)
			if (file.endswith("Tests.dll") and pattern.search(pathAndFile) != None):
				testDlls.append(pathAndFile)
	print("----- Unit Test DLLs -----")
	print('\n'.join(testDlls))
	
	# Exclude assemblies and namespaces from coverage reports. This includes all tests, 
	# plus any third party libraries.
	# Syntax for exclusion is "-[assembly]namespace", where either the namespace or the 
	# assembly can have wildcards
	OpenCoverFilter = "+[*]*"
	OpenCoverFilter += " -[*]Moq.*" 
	OpenCoverFilter += " -[*]Esent.*" 
	OpenCoverFilter += " -[*]GalaSoft.*" 
	OpenCoverFilter += " -[*]System.Diagnostics.Contracts.RuntimeContractsAttribute*" 
	OpenCoverFilter += " -[*]*.GeneratedInternalTypeHelper*" 
	OpenCoverFilter += " -[*]*.Tests" 
	OpenCoverFilter += " -[*.Tests]*" 
	# Exclude specific file types from examination
	ExcludedFiles='*.xaml;*.xaml.cs;*Test;*Test*'

	# Make or clean out coverage directory
	MakeDirectory('Coverage')
	
	# Setup the command to run unit tests with VSTest and OpenCover for coverage analysis.
	testDllsStr = ' '.join(testDlls)
	targetArgs = '{0} /Platform:{1} /Framework:{2} /InIsolation /Logger:trx'.format(
		testDllsStr, testPlatform, testFramework)
	targetArgs = re.sub('"', '\\"', targetArgs)
	opencoverCommand = ' '.join([opencover, '-target:"{0}" -targetargs:"{1}"'
		.format(vstest, targetArgs),
		'-filter:"{0}"'.format(OpenCoverFilter),
		'-excludebyattribute:*.ExcludeFromCodeCoverage*;*.CompilerGeneratedAttribute',
		'-excludebyfile:{0}'.format(ExcludedFiles),
		'-hideskipped:All -output:Coverage\CodeCoverageResultsOpenCover.xml',
		'-register:{0} -mergebyhash -returntargetcode:256'.format(OpenCoverRegister)])
	
	# Invoke test run command
	print("----- Test -----")
	result = RunCommand(opencoverCommand)
	
	if (result[0]==0):
		return CheckTestCoverage(args, result[1])
	return result[0]


# Generate a code coverage report using ReportGenerator.
def GenerateReport(args):
	reportgenerator=(
		'C:/Programs/ReportGenerator/ReportGenerator_2.4.5.0/bin/ReportGenerator.exe')
	
	# Make or clean out coverage report directory
	MakeDirectory('CoverageHTML')
	reportGenCommand = ' '.join([reportgenerator, 
		'-reports:Coverage\CodeCoverageResultsOpenCover.xml',
		'-targetdir:CoverageHTML'])

	# Invoke coverage report generation command
	print()
	print("----- Report Coverage -----")
	result = RunCommand(reportGenCommand)
	return result[0]


# Collect build artifacts into standard locations for zipping and storing on GitLab.
def CollectArtifacts(args):
	if (args['ARCHIVEBUILDFROM']!=''):
		sourceDir = args['ARCHIVEBUILDFROM']
		destDir = 'Artifacts/Build'
		shutil.copytree(sourceDir, destDir, 
			ignore=shutil.ignore_patterns('*.pdb', '*.vshost.*', '*.wixpdb'))
	if (args['ARCHIVECOVERAGE']):
		sourceDir = 'CoverageHTML'
		destDir = 'Artifacts/Coverage'
		shutil.copytree(sourceDir, destDir)
	shutil.copyfile('Manifest.json', 'Artifacts/Build/Manifest.json')

# Fetch all the nuget packages specified through the solution file
def FetchNugetPackages(args):
	nuget='C:/Program Files (x86)/NuGet/nuget.exe'
	
	nugetCommand='"{0}" restore "{1}"'.format(nuget, args['SOLUTION'])
	print("----- Fetch Nuget Packages -----")
	result = RunCommand(nugetCommand)
	return result[0]
	

# Create a nuget package from a recently built solution	
def CreateNugetPackage(args):
	nuget='C:/Program Files (x86)/NuGet/nuget.exe'
	tensentricLibrary=r'\\192.168.111.28\SoftwareReleases\NuGet'
	nugetFile='"{0}/{1}/{1}.nuspec"'.format(args['SOLUTIONPATH'], args['NUGETPROJECT'])
	binDir='"{0}/{1}/bin/release"'.format(args['SOLUTIONPATH'], args['NUGETPROJECT'])
	outputDir='Artifacts/Nuget'
	version=args['VERSION']
	MakeDirectory(outputDir)
	nugetCommand=(
		'"{0}" pack {1} -version {2} -basepath {3} -exclude *.pdb -outputdirectory {4}'
		.format(nuget, nugetFile, version, binDir, outputDir))
	print("----- Build Nuget Package -----")
	result = RunCommand(nugetCommand)
	if (result[0]!=0):
		return result[0]
	# NuGet leaves trailing 0s off the end of its file versions
	versionName = re.sub(r'(\.0)+$', '', version)
	package='{0}/{1}.{2}.nupkg'.format(outputDir, args['NUGETPROJECT'], versionName)
	publishCommand='"{0}" add {1} -source {2}'.format(nuget, package, tensentricLibrary)
	print("----- Publish Nuget Package -----")
	result = RunCommand(publishCommand)
	return result[0]


# ------------------------------ Version Numbers ----------------------------------------

	
# Set the version string, based on command line or environment variable input, 
# falling back to "0.0.0.0"	if no information.
def SetVersion(args):
	tag=args['BUILDTAG']
	build=args['BUILDID']
	if (tag==''):
		if ('CI_BUILD_REF_NAME' in os.environ):
			tag=os.environ['CI_BUILD_REF_NAME']
	if (build==''):
		if ('CI_BUILD_ID' in os.environ):
			build=os.environ['CI_BUILD_ID']
			# Max build ID is UInt16.MaxValue - 1
			if (int(build) > 65534):
				build='0'
		else:
			build='0'
	versionPattern=re.compile(r'(\d+\.){0,2}\d+')
	match=versionPattern.search(tag)
	if (match != None):
		versionBase=match[0]
		while (versionBase.count('.') < 2):
			versionBase = versionBase + '.0'
	else:
		versionBase="0.0.0"
	version='{0}.{1}'.format(versionBase, build)
	print('----------------------')
	print('Version: {0}'.format(version))
	args['VERSION']=version
	
	
# Update the version strings in the named file
def UpdateVersion(file, newVersion):
	print('Updating: {0}'.format(file))
	pattern=r'Version\("[\d\.]+"\)]'
	output='Version("{0}")]'.format(newVersion)
	infile=open(file, "r")
	text=infile.read()
	infile.close()
	updatedText=re.sub(pattern, output, text)
	outfile=open(file, "w")
	outfile.write(updatedText)
	outfile.close()
	
def UpdateWixVersionFile(file, newVersion):
	print('Updating: {0}'.format(file))
	pattern=r'BuildVersion = 0.0.0.0'
	output='BuildVersion = {0}'.format(newVersion)
	infile=open(file, "r")
	text=infile.read()
	infile.close()
	updatedText=re.sub(pattern, output, text)
	outfile=open(file, "w")
	outfile.write(updatedText)
	outfile.close()

def CreateManifestFile(newVersion):
	data = {}  
	data['version'] = '{0}'.format(newVersion)
	data['url'] = 'http://sync.nullbits.co/downloads/Sync_v{0}.zip'.format(newVersion)
	data['changelog'] = ""
	data['madatory'] = False

	with open('Manifest.json', 'w') as outfile:
		json.dump(data, outfile, indent=4)
	return 0

# Find all C# AssemblyInfo files in the solution, and update the versions
# specified in each.	
def UpdateCSharpVersions(args):
	try:
		files = GetAllVersionFiles(args['SOLUTIONPATH'], 'AssemblyInfo.cs')
		for file in files:
			UpdateVersion(file, args['VERSION'])
	except: 
		print("Error while updating version numbers.")
		return 1
	return 0
	
def UpdateWixVersion(args):
	try:
		files = GetAllVersionFiles(args['SOLUTIONPATH'], 'Product.wxs')
		for file in files:
			UpdateWixVersionFile(file, args['VERSION'])
	except: 
		print("Error while updating wix version numbers.")
		return 1
	return 0

# ---------------------------------- Main Program ---------------------------------------
	
	
def Main():	
	# Debug
	#for key in os.environ:
	#	print('{0} = {1}'.format(key, os.environ[key]))
	
	args = ParseArguments(sys.argv)
	SetVersion(args)
	returnCode = 0
	MakeDirectory('Artifacts')
	if (args['NUGETFETCH']):
		returnCode = FetchNugetPackages(args)
	if (returnCode==0):
		returnCode = UpdateCSharpVersions(args)
	if (returnCode==0):
		returnCode = UpdateWixVersion(args)
	if (returnCode==0):
		returnCode = CreateManifestFile(args['VERSION'])
	if (returnCode==0):
		returnCode = Build(args)
	if ((returnCode==0) and (args['TEST'])):
		returnCode = RunTests(args)
	if ((returnCode==0) and (args['COVER'])):
		returnCode = GenerateReport(args)
	if ((returnCode==0) and (args['NUGETPROJECT']!='')):
		returnCode = CreateNugetPackage(args)
	
	CollectArtifacts(args)
	return returnCode

	
# TODO: Consider wrapping the call to main in a try block, for better debug output.	
returnCode = Main()
sys.exit(returnCode)