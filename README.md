# Sync App

The DB1 Sync app is an application written in WPF using the MVVM design patterns with Ninject and Reactive Properties. 

This app allows users to update DB1 Pro firmware, change LED patterns, and bind the keys to desired keycodes and macros.

The app uses [HID API](https://github.com/signal11/hidapi) to communicate with our custom messaging protocol.

# Not on Windows?

Check out [db1-python](https://gitlab.com/davidcerrone/db1-python) to do all the same functionality on Mac and Linux. 
Or for the more advanced users, use the Python API to update your DB1 LEDs on the fly for game integration, weather, or whatever you'd like! The possibilities are endless.


# DB1 Firmware

The DB1 firmware is written using the open source [QMK](https://github.com/qmk/qmk_firmware) firmware for keyboards. 
If you want to hack your own firmware, or update to the latest build, check out [firmware update](https://nullbits.co/update/) for detailed instructions.

# App Update

You can find the latest tagged build [here](https://gitlab.com/davidcerrone/db1-sync/tags). Download the artificts, unzip the folder, and run Setup.msi.

# Issues

You can submit issues with the application [here](https://gitlab.com/davidcerrone/db1-sync/issues).

# Known issues

* In-application udpates are a known issue at the moment. 