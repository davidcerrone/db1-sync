﻿using System;
using System.Globalization;
using System.Runtime.ExceptionServices;
using System.Threading;

namespace HidApi
{
    public class DeviceScanner
    { 
        public event EventHandler DeviceArrived;
        public event EventHandler DeviceRemoved;

        public bool IsDeviceConnected
        {
            get { return _deviceConnected; }
        }

        // for async reading
        private object _syncLock = new object();
        private Thread _scannerThread;
        private volatile bool _asyncScanOn = false;

        private volatile bool _deviceConnected = false;

        private int _scanIntervalMillisecs = 500;
        public int ScanIntervalInMillisecs
        {
            get { lock (_syncLock) { return _scanIntervalMillisecs; } }
            set { lock (_syncLock) { _scanIntervalMillisecs = value; } }
        }

        public bool IsScanning
        {
            get { return _asyncScanOn; }
        }

        private ushort _vendorId;
        private ushort _productId;

        // Use this class to monitor when your devices connects.
        // Note that scanning for device when it is open by another process will return FALSE
        // even though the device is connected (because the device is unavailiable)
        public DeviceScanner(ushort VendorID, ushort ProductID, int scanIntervalMillisecs = 100)
        {
            _vendorId = VendorID;
            _productId = ProductID;
            ScanIntervalInMillisecs = scanIntervalMillisecs;
        }

        // scanning for device when it is open by another process will return false
        public static bool ScanOnce(ushort vid, ushort pid)
        {
            return HidApi.hid_enumerate(vid, pid) != IntPtr.Zero;
        }

        public void StartAsyncScan()
        {
            // Build the thread to listen for reads
            if (_asyncScanOn)
            {
                // dont run more than one thread
                return;
            }
            _asyncScanOn = true;
            _scannerThread = new Thread(ScanLoop);
            _scannerThread.Name = "HidApiAsyncDeviceScanThread";
            _scannerThread.Start();
        }

        public void StopAsyncScan()
        {
            _asyncScanOn = false;
        }

        [HandleProcessCorruptedStateExceptions]
        private void ScanLoop()
        {
            var culture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // The read has a timeout parameter, so every X milliseconds
            // we check if the user wants us to continue scanning.
            while (_asyncScanOn)
            {
                try
                {
                    IntPtr deviceInfo = HidApi.hid_enumerate(_vendorId, _productId);
                    bool deviceOnBus = deviceInfo != IntPtr.Zero;
                    // freeing the enumeration releases the device, 
                    // do it as soon as you can, so we dont block device from others
                    HidApi.hid_free_enumeration(deviceInfo);
                    if (deviceOnBus && ! _deviceConnected)
                    {
                        // just found new device
                        _deviceConnected = true;
                        if (DeviceArrived != null)
                        {
                            DeviceArrived(this, EventArgs.Empty);
                        }
                    }
                    if (! deviceOnBus && _deviceConnected)
                    {
                        // just lost device connection
                        _deviceConnected = false;
                        if (DeviceRemoved != null)
                        {
                            DeviceRemoved(this, EventArgs.Empty);
                        }
                    }
                }
                catch (System.AccessViolationException ex)
                {
                    // ignore?
                    Console.WriteLine("Hit the AccessViolationException");
                }
                catch (Exception)
                {
                    // stop scan, user can manually restart again with StartAsyncScan()
                    _asyncScanOn = false;
                }                
                // when read 0 bytes, sleep and read again
                Thread.Sleep(ScanIntervalInMillisecs);
            }
        }
    }
}
