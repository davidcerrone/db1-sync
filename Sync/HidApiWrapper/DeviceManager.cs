﻿using System;
using System.Runtime.InteropServices;

namespace HidApi
{
    public class DeviceManager
    {
        public const int CrSuccess = 0x00000000;
        public const int CmLocateDevnodeNormal = 0x00000000;
        public const int DigcfPresent = 0x00000002;
        public const int DifRemove = 0x00000005;

        // devnode info struct
        [StructLayout(LayoutKind.Sequential)]
        public class SpDevinfoData
        {
            public int cbSize;
            public Guid ClassGuid;
            public int DevInst;

            public ulong Reserved;
        }

        // importing external SetupAPI methods
        [DllImport("cfgmgr32.dll")]
        public static extern UInt32 CM_Locate_DevNode(ref UInt32 devInst, string pDeviceId, UInt32 flags);

        [DllImport("cfgmgr32.dll", SetLastError = true)]
        public static extern UInt32 CM_Reenumerate_DevNode(UInt32 devInst, UInt32 flags);

        [DllImport("setupapi.dll")]
        public static extern Boolean SetupDiClassGuidsFromNameA(string className, ref Guid guids, UInt32 classNameSize, ref UInt32 requiredSize);

        [DllImport("setupapi.dll")]
        public static extern IntPtr SetupDiGetClassDevsA(ref Guid classGuid, UInt32 enumerator, IntPtr hwndPtr, UInt32 flags);

        [DllImport("setupapi.dll")]
        public static extern Boolean SetupDiEnumDeviceInfo(IntPtr deviceInfoSet, UInt32 deviceIndex, SpDevinfoData deviceInfoData);

        [DllImport("setupapi.dll", SetLastError = true)]
        public static extern Boolean SetupDiCallClassInstaller(UInt32 installFunction, IntPtr deviceInfoSet, SpDevinfoData deviceInfoData);

        [DllImport("setupapi.dll")]
        public static extern Boolean SetupDiDestroyDeviceInfoList(IntPtr deviceInfoSet);

        public Int32 RemoveAllDevicesInClassName(string className)
        {
            IntPtr newDeviceInfoSet;
            UInt32 requiredSize = 0;
            SpDevinfoData deviceInfoData = new SpDevinfoData();
            Guid[] guids = new Guid[1];

            // getting required size to store Guids for given class name
            Boolean result = SetupDiClassGuidsFromNameA(
                className, ref guids[0], requiredSize, ref requiredSize);
            if (requiredSize == 0)
            {
                // incorrect class name
                return -4;
            }
            if (!result)
            {
                guids = new Guid[requiredSize];
                // getting the actual Guids
                result = SetupDiClassGuidsFromNameA(
                    className, ref guids[0], requiredSize, ref requiredSize);
            }
            if (!result)
            {
                // incorrect class name
                return -4;
            }
            // getting all present devnodes for given Guid
            newDeviceInfoSet = SetupDiGetClassDevsA(ref guids[0], 0, IntPtr.Zero, DigcfPresent);

            if (newDeviceInfoSet.ToInt32() == -1)
            {
                // unavailable
                return -8;
            }

            // preparing device info struct
            deviceInfoData.cbSize = 28;
            deviceInfoData.ClassGuid = Guid.Empty;
            deviceInfoData.DevInst = 0;
            deviceInfoData.Reserved = 0;

            UInt32 i;
            // for each device in class (set of devnodes pointed by NewDeviceInfoSet)
            for (i = 0; SetupDiEnumDeviceInfo(newDeviceInfoSet, i, deviceInfoData); i++)
            {
                // invoke class installer method with DIF_REMOVE flag
                if (!SetupDiCallClassInstaller(DifRemove, newDeviceInfoSet, deviceInfoData))
                {
                    // failed to uninstall device
                    SetupDiDestroyDeviceInfoList(newDeviceInfoSet);
                    return -16;
                }
            }
            // perform cleanup
            SetupDiDestroyDeviceInfoList(newDeviceInfoSet);
            return CrSuccess;
        }

        public Int32 ScanForHardwareChanges()
        {
            UInt32 devInst = 0;
            UInt32 result = CM_Locate_DevNode(ref devInst, null, CmLocateDevnodeNormal);
            if (result != CrSuccess)
            {
                // failed to get devnode
                return -1;
            }
            result = CM_Reenumerate_DevNode(devInst, 0);

            if (result != CrSuccess)
            {
                // reenumeration failed
                return -2;
            }
            return CrSuccess;
        }
    }
}
