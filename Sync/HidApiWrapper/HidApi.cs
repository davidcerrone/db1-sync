﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace HidApi
{
    internal class HidApi
    {
        #region Native Methods

        [StructLayout(LayoutKind.Sequential)]
        public struct hid_device_info
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public String path;
            public ushort vendor_id;
            public ushort product_id;
            [MarshalAs(UnmanagedType.LPWStr)]
            public String serial_number;
            public ushort release_number;
            [MarshalAs(UnmanagedType.LPWStr)]
            public String manufacturer_string;
            [MarshalAs(UnmanagedType.LPWStr)]
            public String product_string;
            public ushort usage_page;
            public ushort usage;
            public int interface_number;
            public IntPtr next;
        };

        // On windows for system installed: hidapi.dll
        // On linux for system installed: "libhidapi-hidraw" or "libhidapi-libusb"
        // unfortunately there is no way simple to automatically
        // find the library on all platforms becasue of different
        // naming conventions.
        // Just use hidapi and expect users to supply it in same folder as .exe
        public const string DllFileName = "hidapi";

        /// Return Type: int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_init();


        /// Return Type: int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_exit();

        /// Return Type: hid_device_info*
        ///vendor_id: unsigned short
        ///product_id: unsigned short
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr hid_enumerate(ushort vendorId, ushort productId);

        /// Return Type: void
        ///devs: struct hid_device_info*
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
		public static extern void hid_free_enumeration(IntPtr devs);

        /// Return Type: hid_device*
        ///vendor_id: unsigned short
        ///product_id: unsigned short
        ///serial_number: wchar_t*
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr hid_open(ushort vendorId, ushort productId, [In] string serialNumber);


        /// Return Type: hid_device*
        ///path: char*
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr hid_open_path([In] string path);


        /// Return Type: int
        ///device: hid_device*
        ///data: unsigned char*
        ///length: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_write(IntPtr device, [In] byte[] data, uint length);


        /// Return Type: int
        ///dev: hid_device*
        ///data: unsigned char*
        ///length: size_t->unsigned int
        ///milliseconds: int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_read_timeout(IntPtr device, [Out] byte[] bufData, uint length, int milliseconds);


        /// Return Type: int
        ///device: hid_device*
        ///data: unsigned char*
        ///length: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_read(IntPtr device, [Out] byte[] bufData, uint length);


        /// Return Type: int
        ///device: hid_device*
        ///nonblock: int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public extern static int hid_set_nonblocking(IntPtr device, int nonblock);


        /// Return Type: int
        ///device: hid_device*
        ///data: char*
        ///length: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_send_feature_report(IntPtr device, [In] byte[] data, uint length);


        /// Return Type: int
        ///device: hid_device*
        ///data: unsigned char*
        ///length: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public static extern int hid_get_feature_report(IntPtr device, [Out] byte[] bufData, uint length);


        /// Return Type: void
        ///device: hid_device*
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl)]
        public extern static void hid_close(IntPtr device);


        /// Return Type: int
        ///device: hid_device*
        ///string: wchar_t*
        ///maxlen: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        public static extern int hid_get_manufacturer_string(IntPtr device, StringBuilder bufString, uint length);


        /// Return Type: int
        ///device: hid_device*
        ///string: wchar_t*
        ///maxlen: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        public static extern int hid_get_product_string(IntPtr device, StringBuilder bufString, uint length);


        /// Return Type: int
        ///device: hid_device*
        ///string: wchar_t*
        ///maxlen: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        public static extern int hid_get_serial_number_string(IntPtr device, StringBuilder bufSerial, uint maxlen);


        /// Return Type: int
        ///device: hid_device*
        ///string_index: int
        ///string: wchar_t*
        ///maxlen: size_t->unsigned int
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        public static extern int hid_get_indexed_string(IntPtr device, int stringIndex, StringBuilder bufString, uint maxlen);


        /// Return Type: wchar_t*
        ///device: hid_device*
        [DllImport(DllFileName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        public static extern IntPtr hid_error(IntPtr device);



        #endregion
    }
}
