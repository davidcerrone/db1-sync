﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sync.Utilities;
using Sync.Utilities.Interfaces;
using Sync.ViewModels;
using Sync.ViewModels.Interfaces;
using Ninject.Modules;

namespace Sync.IoC
{
    /// <summary>
    /// Ninject module for this assembly.
    /// </summary>
    public class UserInterfaceModule : NinjectModule
    {
        /// <summary>
        /// Loads the Ninject module type bindings for this assembly.
        /// </summary>
        public override void Load()
        {
            Bind<MainWindow>().ToSelf().InSingletonScope();
            Bind<ILedConfigurationViewModel>().To<LedConfigurationViewModel>();
            Bind<IAboutViewModel>().To<AboutViewModel>();
            Bind<IKeyConfigurationViewModel>().To<KeyConfigurationViewModel>();
            Bind<ISyncListener>().To<SyncListener>().InSingletonScope();
            Bind<MainWindowViewModel>().ToSelf().InSingletonScope();
            Bind<IUpdateUtility>().To<UpdateUtility>().InSingletonScope();
            Bind<DropConfigurationViewModel>().ToSelf().InSingletonScope();
        }
    }
}
