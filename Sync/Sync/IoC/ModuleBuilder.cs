﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace Sync.IoC
{
    /// <summary>
    /// Ninject module builder for this assembly.
    /// </summary>
    public class ModuleBuilder
    {
        /// <summary>
        /// Gets the Ninject module for this assembly.
        /// </summary>
        /// <returns>The Ninject module.</returns>
        public static NinjectModule GetModule()
        {
            return new UserInterfaceModule();
        }
    }
}
