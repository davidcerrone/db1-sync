﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync.Models
{
    public class TetherModel
    {
        public string serial { get; set; }
        public object comments { get; set; }
        public bool success { get; set; }
        public bool unlocked { get; set; }
    }
}
