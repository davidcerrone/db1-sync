﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync
{
    public static class Db1Constants
    {
        public const ushort ProductId = 0xFEED;
        public const ushort VendorId = 0x6060;
        public const ushort UsagePage = 0xFF60;
        public const int ReportLength = 8;
        public const bool HasReportIds = true;
        public const string SerialNumber = null;
    }
}
