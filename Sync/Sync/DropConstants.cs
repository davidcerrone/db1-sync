﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync
{
    public static class DropConstants
    {
        public const ushort ProductId = 0x04D8;
        public const ushort VendorId = 0xEED3;
        public const ushort UsagePage = 0xFF60;
        public const int ReportLength = 8;
        public const bool HasReportIds = false;
        public const string SerialNumber = null; //"1537509295";
    }
}
