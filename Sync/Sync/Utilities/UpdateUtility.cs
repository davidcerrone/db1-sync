﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Sync.Utilities.Interfaces;
using Newtonsoft.Json;

namespace Sync.Utilities
{
    internal class UpdateUtility : IUpdateUtility
    {
        public IObservable<Manifest> ManifestObservable => _manifestSubject.AsObservable();
        public Manifest Manifest { get; private set; }
        public IObservable<bool> InstallCompleteObservable => _installCompleteSubject.AsObservable();
        private Subject<bool> _installCompleteSubject = new Subject<bool>();
        private readonly Subject<Manifest> _manifestSubject = new Subject<Manifest>();
        private string _filename;
        private const string TempDirectory = "./temp";

        public UpdateUtility()
        {

        }

        public void QueryUpdateAvailable()
        {
            try
            {
                WebClient client = new WebClient();
                var manifest = client.DownloadString("http://sync.nullbits.co/downloads/Manifest.json");
                var jsonManifest = JsonConvert.DeserializeObject<Manifest>(manifest);
                if (jsonManifest != null)
                {
                    Manifest = jsonManifest;
                    _manifestSubject.OnNext(jsonManifest);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task InstallUpdateAsync()
        {
            try
            {
                if (Manifest == null)
                {
                    return;
                }

                WebClient client = new WebClient();

                Cleanup();

                Directory.CreateDirectory(TempDirectory);

                _filename = Manifest.Url.Split('/').Last();

                await client.DownloadFileTaskAsync(Manifest.Url, $"{TempDirectory}/{_filename}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                var destinationDirectory = Path.GetFileNameWithoutExtension($"{TempDirectory}/{_filename}");
                ZipFile.ExtractToDirectory($"{TempDirectory}/{_filename}", $"{TempDirectory}");
                var msiExecutable = $"{TempDirectory}/{destinationDirectory}/Setup.msi";
                msiExecutable = Path.GetFullPath($"{TempDirectory}/{destinationDirectory}/Setup.msi");
                InstallMsi(msiExecutable);
            }
        }

        private void InstallMsi(string msi)
        {
            Console.WriteLine($"Installing {Path.GetFileName(msi)}...");

            try
            {
                Process process = new Process();
                process.StartInfo.UseShellExecute = true;
                process.StartInfo.FileName = "msiexec";
                process.StartInfo.Arguments = $"/i \"{msi}\" /quiet /qn /L*V {"./InstallerLog.log"} REBOOT=ReallySuppress";
                process.StartInfo.Verb = "runas";

                process.Start();
                process.WaitForExit();

                _installCompleteSubject.OnNext(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Cleanup();
        }

        private void Cleanup()
        {
            try
            {
                if (Directory.Exists(TempDirectory))
                {
                    Directory.Delete(TempDirectory, true);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
