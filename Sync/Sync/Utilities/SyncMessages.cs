﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync.Utilities
{
    public class SyncMessages
    {
        /// <summary>
        /// HID Protocol 3 (tether lock specific)
        /// </summary>
        public enum MessageId : byte
        {
            ProtocolVersion = 0x01,
            JumpToBootloader,

            SetRgbMode,
            SetRgbHsv,
            SetRgbHsvNoEeprom,

            IncreaseBrightness,
            DecreaseBrightness,

            GetDynamicKeymap,
            SetDynamicKeymap,

            GetMuteLedIsEnabled,
            SetMuteLedIsEnabled,

            SetMacro,
            GetMacro,

            GetLockStatus,
            Unlock,
            GetChipId
        }

        public enum DropMessageId : byte
        {
            ProtocolVersion = 0x01,
            JumpToBootloader,

            IncreaseBrightness,
            DecreaseBrightness,

            IncreasePattern,
            DecreasePattern,

            IncreaseSpeed,
            DecreaseSpeed,

            SetRgbHsvNoEeprom,

            ToggleMode,
        }

        public enum RgbMode : byte
        {
            Invalid,
            
            Static,

            Breathing1,
            Breathing2,
            Breathing3,
            Breathing4,

            RainbowMood1,
            RainbowMood2,
            RainbowMood3,

            RainbowSwirl1,
            RainbowSwirl2,
            RainbowSwirl3,
            RainbowSwirl4,
            RainbowSwirl5,
            RainbowSwirl6,

            Snake1,
            Snake2,
            Snake3,
            Snake4,
            Snake5,
            Snake6,

            Knight1,
            Knight2,
            Knight3,

            Xmas,

            StaticRainbow1,
            StaticRainbow2,
            StaticRainbow3,
            StaticRainbow4,
            StaticRainbow5,
            StaticRainbow6,
            StaticRainbow7,
            StaticRainbow8,
            StaticRainbow9,
            StaticRainbow10,
        }

        [Flags]
        public enum Modifiers : byte
        {
            Alt = 1,
            Ctrl = 2,
            Shift = 4,
        }

        public static byte[] MakeRgbModeMessage(RgbMode rgbMode)
        {
            return new[] {(byte)MessageId.SetRgbMode, (byte)rgbMode};
        }

        public static byte[] MakeHueMessage(ushort hue, byte saturation, byte brightness)
        {
            byte[] buf = new byte[4];
            ArrayFormatter.WriteU16(hue, buf, 0);
            buf[2] = saturation;
            buf[3] = brightness;
            return new[] { (byte)MessageId.SetRgbHsv, buf[0], buf[1], buf[2], buf[3] };
        }

        public static byte[] MakeIncreaseBrightnessMessage()
        {
            return new[] {(byte) MessageId.IncreaseBrightness};
        }

        public static byte[] MakeDecreaseBrightnessMessage()
        {
            return new[] { (byte)MessageId.DecreaseBrightness };
        }

        public static byte[] MakeSetDynamicKeymapMessage(byte layer, byte row, byte column, ushort keycode)
        {
            byte[] buf = new byte[6];
            buf[0] = (byte) MessageId.SetDynamicKeymap;
            buf[1] = layer;
            buf[2] = row;
            buf[3] = column;
            ArrayFormatter.WriteU16(keycode, buf, 4);
            return buf;
        }

        public static byte[] MakeGetDynamicKeymapMessage(byte layer, byte row, byte column)
        {
            byte[] buf = new byte[6];
            buf[0] = (byte)MessageId.GetDynamicKeymap;
            buf[1] = layer;
            buf[2] = row;
            buf[3] = column;
            return buf;
        }

        public static byte[] MakeJumpToBootloaderMessage()
        {
            return new[] {(byte) MessageId.JumpToBootloader};
        }

        public static byte[] MakeSetMuteLedIsEnabledMessage(bool isEnabled)
        {
            if (isEnabled)
            {
                return new byte[] {(byte) MessageId.SetMuteLedIsEnabled, 0x01};
            }
            return new byte[] { (byte) MessageId.SetMuteLedIsEnabled, 0x00 };
        }

        public static byte[] MakeGetMuteLedIsEnabledMessage()
        {
            return new[] { (byte)MessageId.GetMuteLedIsEnabled };
        }

        public static byte[] MakeSetMacroMessage(ushort keycode, string macro, byte modifiers)
        {
            var macroBytes = Encoding.ASCII.GetBytes(macro);
            byte[] buf = new byte[32];

            buf[0] = (byte) MessageId.SetMacro;
            ArrayFormatter.WriteU16(keycode, buf, 1);
            buf[3] = modifiers;
            for (int i = 0; i < macroBytes.Length; i++)
            {
                buf[i + 4] = macroBytes[i];
            }

            return buf;
        }

        public static byte[] MakeGetMacroMessage(ushort keycode)
        {
            byte[] buf = new byte[3];
            buf[0] = (byte)MessageId.GetMacro;
            ArrayFormatter.WriteU16(keycode, buf, 1);
            return buf;
        }

        public static byte[] MakeGetLockStatusMessage()
        {
            return new byte[] { (byte)MessageId.GetLockStatus };
        }

        public static byte[] MakeGetChipIdMessage()
        {
            return new byte[] { (byte)MessageId.GetChipId };
        }

        public static byte[] MakeUnlockMessage(uint key)
        {
            byte a = (byte)((key >> 0) & 0xFF);
            byte b = (byte)((key >> 8) & 0xFF);
            byte c = (byte)((key >> 16) & 0xFF);
            byte d = (byte)((key >> 24) & 0xFF);
            return new byte[] { (byte)MessageId.Unlock, a, b, c, d };
        }
    }
}
