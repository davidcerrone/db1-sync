﻿using HidApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync.Utilities
{
    public static class Db1SyncUtilities
    {
        public static string GetDeviceDescription()
        {
            string description = string.Empty;

            try
            {
                using (var device = new UsbDevice(
                    Db1Constants.ProductId,
                    Db1Constants.VendorId,
                    Db1Constants.SerialNumber,
                    Db1Constants.UsagePage,
                    Db1Constants.HasReportIds,
                    Db1Constants.ReportLength))
                {
                    description = device.Description();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return description;
        }

        public static bool Write(byte[] data)
        {
            bool ok = true;
            byte[] dataToWrite = new byte[33];

            for (int i = 0; i < data.Length; i++)
            {
                dataToWrite[i + 1] = data[i];
            }

            try
            {
                using (var device = new UsbDevice(
                    Db1Constants.ProductId,
                    Db1Constants.VendorId,
                    Db1Constants.SerialNumber,
                    Db1Constants.UsagePage,
                    Db1Constants.HasReportIds,
                    Db1Constants.ReportLength))
                {
                    device.Write(dataToWrite);
                }
            }
            catch (Exception e)
            {
                ok = false;
                Console.WriteLine(e.Message);
            }

            return ok;
        }

        public static bool WriteRead(byte[] data, out byte[] readData, int reportLength = 0)
        {
            bool ok = true;

            byte[] dataToWrite = new byte[33];

            for (int i = 0; i < data.Length; i++)
            {
                dataToWrite[i + 1] = data[i];
            }

            try
            {
                using (var device = new UsbDevice(
                    Db1Constants.ProductId,
                    Db1Constants.VendorId,
                    Db1Constants.SerialNumber,
                    Db1Constants.UsagePage,
                    Db1Constants.HasReportIds,
                    Db1Constants.ReportLength))
                {
                    device.Write(dataToWrite);
                    device.ReadTimeoutInMillisecs = 100;
                    device.ReadIntervalInMillisecs = 100;
                    if (reportLength > 0)
                    {
                        readData = device.Read(reportLength);
                    }
                    else
                    {
                        readData = device.Read();
                    }
                }
            }
            catch (Exception e)
            {
                ok = false;
                readData = null;
                Console.WriteLine(e.Message);
            }

            return ok;
        }
    }
}
