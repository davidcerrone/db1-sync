﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Sync.Utilities
{
    public enum SyncKey
    {
        Key1, // Row 0 Col 1
        Key2, // Row 1 Col 1
        Key3, // Row 1 Col 0
        Key4, // Row 0 Col 0
        Encoder, // Row 0 Col 2
    }

    public class KeyConfiguration
    {
        public SyncKey Key { get; }
        public byte Row { get; }
        public byte Column { get; }
        public byte Layer { get; }
        public ushort DefaultKey { get; }
        public ushort MusicKey { get; }

        public KeyConfiguration(SyncKey key, byte row, byte column, byte layer, ushort defaultKey = 0, ushort musicKey = 0)
        {
            Key = key;
            Row = row;
            Column = column;
            Layer = layer;
            DefaultKey = defaultKey;
            MusicKey = musicKey;
        }
    }
}
