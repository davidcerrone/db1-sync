﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using Sync.Utilities.Interfaces;
using HidApi;
using Reactive.Bindings.Extensions;

namespace Sync.Utilities
{
    internal class SyncListener : ISyncListener
    {
        public IObservable<bool> IsConnected => _isConnected.AsObservable();

        private readonly BehaviorSubject<bool> _isConnected;

        private readonly CompositeDisposable _disposables;

        private DeviceScanner _scanner;

        public SyncListener(CompositeDisposable disposables)
        {
            _disposables = disposables;

            _isConnected = new BehaviorSubject<bool>(DeviceScanner.ScanOnce(Db1Constants.ProductId, Db1Constants.VendorId)).AddTo(_disposables);

            ListenForEvents();
        }

        private void ListenForEvents()
        {
            _scanner = new DeviceScanner(Db1Constants.ProductId, Db1Constants.VendorId, 2000);
            _scanner.DeviceArrived += DeviceEnter;
            _scanner.DeviceRemoved += DeviceExit;
            _scanner.ScanIntervalInMillisecs = 2000;
            _scanner.StartAsyncScan();
        }

        public void DeviceEnter(object s, EventArgs a)
        {
            _isConnected.OnNext(true);
        }
        public void DeviceExit(object s, EventArgs a)
        {
            _isConnected.OnNext(false);
        }

        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                    _scanner.StopAsyncScan();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
