﻿using System;

namespace Sync.Utilities
{
    /// <summary>
    /// Methods to read and write values to an array
    /// </summary>
    public static class ArrayFormatter
    {
        /// <summary>
        /// Read a 32-bit unsigned integer from the array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static uint ReadU32(byte[] array, uint index)
        {
            if ((index >= (uint.MaxValue - 3)) || (index + 3) >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            uint value = (uint) array[index] << 24 |
                         (uint) array[index + 1] << 16 |
                         (uint) array[index + 2] << 8 |
                         (uint) array[index + 3];

            return value;
        }

        /// <summary>
        /// Write a 32-bit unsigned integer to the array
        /// </summary>
        /// <param name="value"></param>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public static void WriteU32(uint value, byte[] array, uint index)
        {
            if ((index >= (uint.MaxValue - 3)) || (index + 3) >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            array[index] = (byte) (value >> 24);
            array[index + 1] = (byte) (value >> 16);
            array[index + 2] = (byte) (value >> 8);
            array[index + 3] = (byte) value;
        }

        /// <summary>
        /// Read a 24-bit unsigned integer from the array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static uint ReadU24(byte[] array, uint index)
        {
            if ((index >= (uint.MaxValue - 2)) || (index + 2) >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            uint value = (uint) array[index] << 16 |
                         (uint) array[index + 1] << 8 |
                         (uint) array[index + 2];

            return value;
        }

        /// <summary>
        /// Read a 16-bit unsigned integer from the array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static ushort ReadU16(byte[] array, uint index)
        {
            if ((index >= (uint.MaxValue - 1)) || (index + 1) >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            ushort value = array[index];
            value <<= 8;
            value |= array[index + 1];

            return value;
        }

        /// <summary>
        /// Write a 16-bit unsigned integer to the array
        /// </summary>
        /// <param name="value"></param>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public static void WriteU16(ushort value, byte[] array, uint index)
        {
            if ((index >= (uint.MaxValue - 1)) || (index + 1) >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            array[index] = (byte) (value >> 8);
            array[index + 1] = (byte) value;
        }

        /// <summary>
        /// Read a 32-bit floating point value from the array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static float ReadFloat(byte[] array, uint index)
        {
            uint valueU32 = ReadU32(array, index);
            byte[] bytes = BitConverter.GetBytes(valueU32);
            float valueF = BitConverter.ToSingle(bytes, 0);
            return valueF;
        }

        /// <summary>
        /// Write a 32-bit floating point value to the array
        /// </summary>
        /// <param name="value"></param>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public static void WriteFloat(float value, byte[] array, uint index)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            uint valueU32 = BitConverter.ToUInt32(bytes, 0);
            WriteU32(valueU32, array, index);
        }
    }
}