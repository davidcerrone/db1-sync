﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;

namespace Sync.Utilities
{
    public enum MacroType
    {
        Type,
        Press,
        Release,
        Wait,
        Interval
    }
    public class MacroCommandObjectViewModel
    {
        public ReactiveProperty<MacroType> MacroType { get; }
        public ReactiveProperty<string> MacroValueString { get; }
        public ReactiveProperty<byte> MacroValue { get; }

        public MacroCommandObjectViewModel(
            ReactiveProperty<MacroType> macroType, 
            ReactiveProperty<byte> macroValue, 
            ReactiveProperty<string> macroValueString)
        {
            MacroType = macroType;
            MacroValue = macroValue;
            MacroValueString = macroValueString;
        }
    }
}
