﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync.Utilities.Interfaces
{
    public interface ISyncListener : IDisposable
    {
        IObservable<bool> IsConnected { get; }
    }
}
