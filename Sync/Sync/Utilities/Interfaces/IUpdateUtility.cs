﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sync.Utilities.Interfaces
{
    public class Manifest
    {
        public Version Version { get; }
        public string Url { get; }
        public string ChangeLog { get; }
        public bool IsMandatory { get; }

        public Manifest(Version version, string url, string changeLog, bool isMandatory)
        {
            Version = version;
            Url = url;
            ChangeLog = changeLog;
            IsMandatory = isMandatory;
        }
    }

    public interface IUpdateUtility
    {
        IObservable<Manifest> ManifestObservable { get; }
        Manifest Manifest { get; }
        IObservable<bool> InstallCompleteObservable { get; }

        void QueryUpdateAvailable();

        Task InstallUpdateAsync();
    }
}
