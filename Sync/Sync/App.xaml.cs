﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Sync
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ApplicationBootstrapper _bootstrapper;
        
        /// <summary>
        /// Startup event handler.
        /// </summary>
        /// <param name="e">Command line arguments passed into the process as it was launched.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                base.OnStartup(e);

                _bootstrapper = new ApplicationBootstrapper();

                Globals.Bootstrapper = _bootstrapper;

                _bootstrapper.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                this.Shutdown();
            }
        }
    }
}
