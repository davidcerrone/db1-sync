﻿using Ninject;
using Sync.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sync.Views
{
    /// <summary>
    /// Interaction logic for DropConfigurationView.xaml
    /// </summary>
    public partial class DropConfigurationView : UserControl
    {
        public DropConfigurationView()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            else
            {
                DataContext = Globals.Kernel.Get<DropConfigurationViewModel>();
            }
        }
    }
}
