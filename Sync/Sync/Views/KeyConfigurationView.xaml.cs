﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sync.Controls;
using Sync.Utilities;
using Sync.ViewModels.Interfaces;
using Ninject;
using MahApps.Metro.SimpleChildWindow;

namespace Sync.Views
{
    /// <summary>
    /// Interaction logic for KeyConfigurationView.xaml
    /// </summary>
    public partial class KeyConfigurationView : UserControl
    {
        public KeyConfigurationView()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            else
            {
                DataContext = Globals.Kernel.Get<IKeyConfigurationViewModel>();
            }
        }

        private async void SingleKeyButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var result = await Globals.Kernel.Get<MainWindow>().ShowChildWindowAsync<HidKeyboardKeypadUsage>(new SelectKeyCodeWindow()
            {
                IsModal = true,
            });

            ((IKeyConfigurationViewModel)DataContext).SetSelectedKey(result);
        }

        private async void MacroKeyButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var result = await Globals.Kernel.Get<MainWindow>().ShowChildWindowAsync<HidKeyboardKeypadUsage>(new SelectKeyCodeWindow()
            {
                IsModal = true,
            });

            ((IKeyConfigurationViewModel)DataContext).SetSelectedKeyMacro(result);
        }
    }
}
