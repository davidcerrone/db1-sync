﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sync.ViewModels.Interfaces;
using Ninject;

namespace Sync.Views
{
    /// <summary>
    /// Interaction logic for LedConfigurationView.xaml
    /// </summary>
    public partial class LedConfigurationView : UserControl
    {
        public LedConfigurationView()
        {
            InitializeComponent();
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            else
            {
                DataContext = Globals.Kernel.Get<ILedConfigurationViewModel>();
            }
        }
    }
}
