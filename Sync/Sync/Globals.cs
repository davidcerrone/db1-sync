﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace Sync
{
    public static class Globals
    {
        /// <summary>
        /// Ninject kernel.
        /// </summary>
        public static IKernel Kernel { get; set; }

        /// <summary>
        /// Bootstrapper
        /// </summary>
        public static ApplicationBootstrapper Bootstrapper { get; set; }

        public static bool IsEarlyAccess { get; }

        static Globals()
        {
            IsEarlyAccess = true;
        }
    }
}
