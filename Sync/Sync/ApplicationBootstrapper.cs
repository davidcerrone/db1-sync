﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Sync.Utilities;
using Ninject;
using Ninject.Modules;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using Reactive.Bindings.Extensions;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Sync.Models;
using Sync.Utilities.Interfaces;

namespace Sync
{
    public class ApplicationBootstrapper : IDisposable
    {
        private CompositeDisposable _disposables;
        private IKernel _kernel;

        public IObservable<bool> IsLocked => _isLockedSubject.AsObservable();
        private Subject<bool> _isLockedSubject = new Subject<bool>();
        public IObservable<string> SerialNumber => _serialNumberSubject.AsObservable();
        private BehaviorSubject<string> _serialNumberSubject = new BehaviorSubject<string>("");
        private bool _snPublished = false;

        public void Run()
        {
            _disposables = new CompositeDisposable();
            _kernel = BuildKernel();
            _disposables.Add(_kernel);
            Globals.Kernel = _kernel;

            Application.Current.MainWindow = _kernel.Get<MainWindow>();
            Application.Current.MainWindow.Show();

            if (Globals.IsEarlyAccess)
            {
                _kernel.Get<ISyncListener>().IsConnected.Subscribe(
                    isConnected =>
                    {
                        if (isConnected)
                        {
                            UnlockSequence();
                        }
                    }).AddTo(_disposables);                
            }
        }

        private static IKernel BuildKernel()
        {
            List<INinjectModule> modules = new List<INinjectModule>
            {
                Sync.IoC.ModuleBuilder.GetModule()
            };

            return new StandardKernel(modules.ToArray());
        }

        private IDisposable _pingSub = null;

        private void UnlockSequence()
        {
            // Get the chip id and lock status
            Db1SyncUtilities.WriteRead(SyncMessages.MakeGetChipIdMessage(), out var chipId, 11);
            Db1SyncUtilities.WriteRead(SyncMessages.MakeGetLockStatusMessage(), out var lockStatus);

            var sn = ParseSerialNumber(chipId);

            if (!_snPublished)
            {
                _snPublished = true;
                _serialNumberSubject.OnNext(sn);
            }            

            var tetherDetails = GetTetherDetails(sn);
            
            if (tetherDetails != null && tetherDetails.success && tetherDetails.unlocked)
            {
                Db1SyncUtilities.Write(SyncMessages.MakeUnlockMessage(GetHaskKey(chipId, lockStatus)));

                _pingSub?.Dispose();
                _pingSub = Observable.Interval(TimeSpan.FromMilliseconds(1000)).Subscribe(
                    _ =>
                    {
                        if (CheckIfLocked())
                        {
                            UnlockSequence();
                        }
                        else
                        {
                            _isLockedSubject.OnNext(false);
                        }
                    }).AddTo(_disposables);
            }
            else
            {
                _pingSub?.Dispose();
                _isLockedSubject.OnNext(true);
            }
        }

        private uint GetHaskKey(byte[] chipId, byte[] lockStatus)
        {
            ulong mulp = 558481073;
            ulong timeStamp = ParseTimeStamp(lockStatus);
            string serialNumber = ParseSerialNumber(chipId);

            timeStamp ^= 295610356;

            foreach (var c in serialNumber)
            {
                timeStamp += ((c * mulp) ^ (timeStamp >> 12));
            }

            return (uint)((timeStamp ^ (timeStamp << 26)) & 0xFFFFFFFF);
        }

        private string ParseSerialNumber(byte[] chipId)
        {
            string serialNumber = string.Empty;

            try
            {
                for (int i = 1; i < 11; i++)
                {
                    serialNumber += $"{chipId[i]:X2}";
                }
            }
            catch (Exception ex)
            {
            }

            return serialNumber;
        }

        private ulong ParseTimeStamp(byte[] lockStatus)
        {
            ulong timeStamp = 0;

            try
            {
                for (int i = 2; i < 7; i++)
                {
                    timeStamp |= (ulong)(lockStatus[i] << (8 * (i - 2)));
                }
            }
            catch (Exception ex)
            {
            }

            return timeStamp;
        }

        private bool CheckIfLocked()
        {
            Db1SyncUtilities.WriteRead(SyncMessages.MakeGetLockStatusMessage(), out var lockStatus);

            try
            {
                if (lockStatus[1] == 1)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        private TetherModel GetTetherDetails(string sn)
        {
            // Returns JSON string
            string GET(string url)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                try
                {
                    WebResponse response = request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                        return reader.ReadToEnd();
                    }
                }
                catch (WebException ex)
                {
                    WebResponse errorResponse = ex.Response;
                    using (Stream responseStream = errorResponse.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                        String errorText = reader.ReadToEnd();
                        // log errorText
                    }
                    return "";
                }
            }

            var json = GET($"https://tether.nullbits.co/status?sn={sn}");

            try
            {
                return JsonConvert.DeserializeObject<TetherModel>(json);
            }
            catch
            {
                return null;
            }
        }

        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
