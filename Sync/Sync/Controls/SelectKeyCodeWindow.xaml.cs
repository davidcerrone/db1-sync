﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sync.Utilities;
using MahApps.Metro.SimpleChildWindow;

namespace Sync.Controls
{
    /// <summary>
    /// Interaction logic for SelectKeyCodeWindow.xaml
    /// </summary>
    public partial class SelectKeyCodeWindow : ChildWindow
    {
        public SelectKeyCodeWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is Button button)
                    this.Close(Enum.Parse(typeof(HidKeyboardKeypadUsage), button.Name));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
