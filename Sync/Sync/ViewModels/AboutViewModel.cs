﻿using System;
using System.Reactive.Disposables;
using System.Reflection;
using System.Security.Principal;
using System.Windows;
using Sync.Utilities;
using Sync.Utilities.Interfaces;
using Sync.ViewModels.Interfaces;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;

namespace Sync.ViewModels
{
    internal class AboutViewModel : IAboutViewModel
    {
        public ReactiveProperty<string> DeviceDescription { get; private set; }
        public ReactiveCommand JumpToBootloaderCommand { get; private set; }
        public ReactiveCommand NullbitsWebsiteRequestedCommand { get; private set; }
        public ReactiveCommand CheckForUpdateCommand { get; private set; }
        public ReactiveCommand InstallUpdateCommand { get; private set; }
        public ReactiveProperty<string> UpdateStatus { get; private set; }
        public ReactiveProperty<Visibility> InstallButtonVisibility { get; private set; }
        public ReactiveProperty<string> SerialNumber { get; private set; }

        private readonly CompositeDisposable _disposables;
        private readonly IUpdateUtility _updateUtility;

        public AboutViewModel(CompositeDisposable disposables, IUpdateUtility updateUtility)
        {
            _disposables = disposables;
            _updateUtility = updateUtility;

            CreateCommandsAndProperties();
        }

        private void CreateCommandsAndProperties()
        {
            var desc = Db1SyncUtilities.GetDeviceDescription();
            desc = desc.Replace("/", "  Compiled on:");
            desc = desc.Replace("\"", " ");
            DeviceDescription = new ReactiveProperty<string>(desc)
                .AddTo(_disposables);

            UpdateStatus = new ReactiveProperty<string>()
                .AddTo(_disposables);

            InstallButtonVisibility = new ReactiveProperty<Visibility>(Visibility.Hidden)
                .AddTo(_disposables);

            JumpToBootloaderCommand = (ReactiveCommand) new ReactiveCommand()
                .WithSubscribe(_ => Db1SyncUtilities.Write(SyncMessages.MakeJumpToBootloaderMessage()))
                .AddTo(_disposables);

            NullbitsWebsiteRequestedCommand = new ReactiveCommand()
                .WithSubscribe(OpenNullbitsWebsite)
                .AddTo(_disposables);

            CheckForUpdateCommand = new ReactiveCommand()
                .WithSubscribe(CheckForUpdate)
                .AddTo(_disposables);

            InstallUpdateCommand = new ReactiveCommand()
                .WithSubscribe(InstallUpdate)
                .AddTo(_disposables);

            Globals
                .Bootstrapper
                .SerialNumber
                .Subscribe(sn => DeviceDescription.Value += $"\nSN: {sn}")
                .AddTo(_disposables);
        }

        private void OpenNullbitsWebsite()
        {
            System.Diagnostics.Process.Start("https://nullbits.co/");
        }

        private void CheckForUpdate()
        {
            _updateUtility.ManifestObservable.Subscribe(
                manifest =>
                {
                    if (manifest.Version > Assembly.GetExecutingAssembly().GetName().Version)
                    {
                        if (!IsRunAsAdmin())
                        {
                            UpdateStatus.Value = $"Version {_updateUtility.Manifest.Version} is available! To update, run application as administrator.";
                            InstallButtonVisibility.Value = Visibility.Hidden;
                        }
                        else
                        {
                            UpdateStatus.Value = $"Version {_updateUtility.Manifest.Version} is available! Install now?";
                            InstallButtonVisibility.Value = Visibility.Visible;
                        }
                    }
                    else
                    {
                        UpdateStatus.Value = $"Up to date!";
                        InstallButtonVisibility.Value = Visibility.Hidden;
                    }
                });

            _updateUtility.QueryUpdateAvailable();
        }

        private void InstallUpdate()
        {
            _updateUtility.InstallUpdateAsync();
        }
        
        private bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);

            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
        
        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
