﻿using System;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using Sync.Utilities;
using System.Reactive.Disposables;
using System.Windows.Media;
using static Sync.Utilities.SyncMessages;

namespace Sync.ViewModels
{
    public class DropConfigurationViewModel
    {
        public ReactiveCommand IncreaseBrightnessCommand { get; private set; }
        public ReactiveCommand DecreaseBrightnessCommand { get; private set; }
        public ReactiveCommand IncreasePatternCommand { get; private set; }
        public ReactiveCommand DecreasePatternCommand { get; private set; }
        public ReactiveCommand IncreaseSpeedCommand { get; private set; }
        public ReactiveCommand DecreaseSpeedCommand { get; private set; }        
        public ReactiveCommand ToggleModeCommand { get; private set; }
        public ReactiveCommand JumpToBootloaderCommand { get; private set; }
        public ReactiveCommand SetHsvCommand { get; private set; }
        public ReactiveProperty<ushort> Hue { get; private set; }
        public ReactiveProperty<byte> Saturation { get; private set; }
        public ReactiveProperty<byte> Value { get; private set; }
        public ReactiveProperty<Color> Color { get; set; }

        private readonly CompositeDisposable _disposables;

        public DropConfigurationViewModel(CompositeDisposable disposables)
        {
            _disposables = disposables;

            CreatePropertiesAndCommands();
        }

        private void CreatePropertiesAndCommands()
        {
            Hue = new ReactiveProperty<ushort>()
                .AddTo(_disposables);
            Saturation = new ReactiveProperty<byte>()
                .AddTo(_disposables);
            Value = new ReactiveProperty<byte>()
                .AddTo(_disposables);
            Color = new ReactiveProperty<Color>(System.Windows.Media.Color.FromRgb(255, 255, 255))
                .AddTo(_disposables);
            Color.Subscribe(_ => SetColorCommand());

            IncreaseBrightnessCommand = new ReactiveCommand()
                .WithSubscribe(IncreaseBrightnessCommandExecute)
                .AddTo(_disposables);
            DecreaseBrightnessCommand = new ReactiveCommand()
                .WithSubscribe(DecreaseBrightnessCommandExecute)
                .AddTo(_disposables);
            IncreasePatternCommand = new ReactiveCommand()
                .WithSubscribe(IncreasePatternCommandExecute)
                .AddTo(_disposables);
            DecreasePatternCommand = new ReactiveCommand()
                .WithSubscribe(DecreasePatternCommandExecute)
                .AddTo(_disposables);
            IncreaseSpeedCommand = new ReactiveCommand()
                .WithSubscribe(IncreaseSpeedCommandExecute)
                .AddTo(_disposables);
            DecreaseSpeedCommand = new ReactiveCommand()
                .WithSubscribe(DecreaseSpeedCommandExecute)
                .AddTo(_disposables);
            SetHsvCommand = new ReactiveCommand()
                .WithSubscribe(SetHsvCommandExecute)
                .AddTo(_disposables);
            ToggleModeCommand = (ReactiveCommand)new ReactiveCommand()
                .WithSubscribe(_ => DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.ToggleMode }))
                .AddTo(_disposables);
            JumpToBootloaderCommand = (ReactiveCommand)new ReactiveCommand()
                .WithSubscribe(_ => DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.JumpToBootloader }))
                .AddTo(_disposables);            
        }

        private void IncreaseBrightnessCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.IncreaseBrightness });
        }

        private void DecreaseBrightnessCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.DecreaseBrightness });
        }

        private void IncreasePatternCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.IncreasePattern });
        }

        private void DecreasePatternCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.DecreasePattern });
        }

        private void IncreaseSpeedCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.IncreaseSpeed });
        }

        private void DecreaseSpeedCommandExecute()
        {
            DropSyncUtilities.Write(new byte[] { (byte)DropMessageId.DecreaseSpeed });
        }

        private void SetHsvCommandExecute()
        {
            // Set value (aka brightness) to max. Brightness button will adjust the value.
            byte[] buf = new byte[4];
            ArrayFormatter.WriteU16(Hue.Value, buf, 0);
            buf[2] = Saturation.Value;
            buf[3] = 255;
            var bufToSend = new[] { (byte)DropMessageId.SetRgbHsvNoEeprom, buf[0], buf[1], buf[2], buf[3] };
            DropSyncUtilities.Write(bufToSend);
        }

        private void SetColorCommand()
        {
            System.Drawing.Color color = System.Drawing.Color.FromArgb(Color.Value.R, Color.Value.G, Color.Value.B);
            Hue.Value = (ushort)color.GetHue();
            Saturation.Value = (byte)(color.GetSaturation() * 255);
            Value.Value = (byte)(color.GetBrightness() * 510);
        }
    }
}
