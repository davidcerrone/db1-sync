﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Sync.Utilities.Interfaces;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.IconPacks;
using Ninject;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using MahApps.Metro.Controls;

namespace Sync.ViewModels
{
    internal class MainWindowViewModel : ViewModelBase, IDisposable
    {
        public ReactiveProperty<string> VersionString { get; private set; }
        public ReactiveCommand NullbitsWebsiteRequestedCommand { get; private set; }
        private IDialogCoordinator _dialogCoordinator;
        private readonly ISyncListener _syncListener;
        private readonly IUpdateUtility _updateUtility;
        private readonly CompositeDisposable _disposables;
        private MetroDialogSettings _dialogSettings;
        private const string NoSyncDialogHeader = "Not Connected";
        private const string NoSyncDialogMessage = "Connect DB1 now";

        private const string UpdateCompleteHeader = "Update Complete";
        private const string UpdateCompleteMessage = "Restart application now";

        private bool _firstOn = true;

        public MainWindowViewModel(ISyncListener syncListener, IDialogCoordinator dialogCoordinator, CompositeDisposable disposables, IUpdateUtility updateUtility)
        {
            _disposables = disposables;
            _syncListener = syncListener;
            _dialogCoordinator = dialogCoordinator;
            _updateUtility = updateUtility;

            _dialogSettings = new MetroDialogSettings();
            _dialogSettings.OwnerCanCloseWithDialog = true;

            NullbitsWebsiteRequestedCommand = new ReactiveCommand()
                .WithSubscribe(OpenNullbitsWebsite)
                .AddTo(_disposables);

            // Build the menus
            Menu.Add(new MenuItem() {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.BoltSolid},
                Text = "LED Configuration",
                NavigationDestination = new Uri("Views/LedConfigurationPage.xaml", UriKind.RelativeOrAbsolute)
            });
            Menu.Add(new MenuItem()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.KeyboardRegular },
                Text = "Button Configuration",
                NavigationDestination = new Uri("Views/KeyConfigurationPage.xaml", UriKind.RelativeOrAbsolute)
            });
            Menu.Add(new MenuItem()
            {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.KeyboardSolid },
                Text = "Drop Configuration",
                NavigationDestination = new Uri("Views/DropConfigurationPage.xaml", UriKind.RelativeOrAbsolute)
            });

            OptionsMenu.Add(new MenuItem() {
                Icon = new PackIconFontAwesome() { Kind = PackIconFontAwesomeKind.InfoCircleSolid },
                Text = "About",
                NavigationDestination = new Uri("Views/AboutPage.xaml", UriKind.RelativeOrAbsolute) });

            Subscribe();

            VersionString = new ReactiveProperty<string>(Assembly.GetExecutingAssembly().GetName().Version.ToString()).AddTo(_disposables);

            if (Globals.IsEarlyAccess)
            {
                VersionString.Value += " - Early Access";
            }
        }

        private void Subscribe()
        {
            _syncListener.IsConnected.ObserveOnDispatcher().Subscribe(
                isConnected =>
                {
                    if (!isConnected)
                    {
                        _firstOn = false;
                        ShowNoSyncDialog();
                    }
                    else
                    {
                        if (_firstOn)
                        {
                            _firstOn = false;
                            return;
                        }
                        ClearDialog();
                    }
                }).AddTo(_disposables);

            _updateUtility.InstallCompleteObservable.ObserveOnDispatcher().Subscribe(
                isComplete =>
                {
                    if (isComplete)
                    {
                        ShowUpdateCompleteDialog();
                    }
                }).AddTo(_disposables);

            Globals.Bootstrapper.IsLocked.ObserveOnDispatcher().Subscribe(
                isLocked =>
                {
                    if (isLocked)
                    {
                        ShowLockedDialog();
                    }
                }).AddTo(_disposables);
        }

        private async void ShowNoSyncDialog()
        {
            ShowDbFlyout();
            // await Globals.Kernel.Get<MainWindow>().ShowMessageAsync(NoSyncDialogHeader, NoSyncDialogMessage);
        }

        private void ShowDbFlyout()
        {
            var flyout = Globals.Kernel.Get<MainWindow>().Flyouts.Items[0] as Flyout;
            if (flyout == null)
            {
                return;
            }

            flyout.IsOpen = !flyout.IsOpen;
        }

        private async void ShowUpdateCompleteDialog()
        {
            await Globals.Kernel.Get<MainWindow>().ShowMessageAsync(UpdateCompleteHeader, UpdateCompleteMessage);
        }

        private async void ShowLockedDialog()
        {
            await Globals.Kernel.Get<MainWindow>().ShowMessageAsync("DB1 Locked", "DB1 EA is complete. Please return to nullbits co.");
        }

        private async void ClearDialog()
        {
            BaseMetroDialog dialogBeingShow = await Globals.Kernel.Get<MainWindow>().GetCurrentDialogAsync<BaseMetroDialog>();

            //Hide Current Single Dialog
            if (dialogBeingShow != null)
            {
                await Globals.Kernel.Get<MainWindow>().HideMetroDialogAsync(dialogBeingShow);
            }
        }

        public object GetItem(object uri)
        {
            return null == uri ? null : this.Menu.FirstOrDefault(m => m.NavigationDestination.Equals(uri));
        }

        public object GetOptionsItem(object uri)
        {
            return null == uri ? null : this.OptionsMenu.FirstOrDefault(m => m.NavigationDestination.Equals(uri));
        }

        private void OpenNullbitsWebsite()
        {
            System.Diagnostics.Process.Start("https://nullbits.co/db1pro/");
        }

        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
