﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Sync.Utilities;
using Sync.ViewModels.Interfaces;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;

namespace Sync.ViewModels
{
    internal class LedConfigurationViewModel : ILedConfigurationViewModel
    {
        public ReactiveProperty<RgbModeUi> RgbMode { get; private set; }
        public ReactiveProperty<bool> MuteLedIsEnabled { get; private set; }
        public ReactiveProperty<ushort> Hue { get; private set; }
        public ReactiveProperty<byte> Saturation { get; private set; }
        public ReactiveProperty<byte> Value { get; private set; }
        public ReactiveProperty<Color> Color { get; set; }
        public ReactiveCommand SetRgbModeCommand { get; private set; }
        public ReactiveCommand SetHsvCommand { get; private set; }
        public ReactiveCommand IncreaseRgbModeSpeedCommand { get; private set; }
        public ReactiveCommand DecreaseRgbModeSpeedCommand { get; private set; }
        public ReactiveCommand IncreaseBrightnessCommand { get; private set; }
        public ReactiveCommand DecreaseBrightnessCommand { get; private set; }

        private byte _rgbMode = 0;

        private readonly CompositeDisposable _disposables;

        public LedConfigurationViewModel(CompositeDisposable disposables)
        {
            _disposables = disposables;
            CreatePropertiesAndCommands();
        }

        private void CreatePropertiesAndCommands()
        {
            RgbMode = new ReactiveProperty<RgbModeUi>(RgbModeUi.Static)
                .AddTo(_disposables);
            Hue = new ReactiveProperty<ushort>()
                .AddTo(_disposables);
            Saturation = new ReactiveProperty<byte>()
                .AddTo(_disposables);
            Value = new ReactiveProperty<byte>()
                .AddTo(_disposables);
            Color = new ReactiveProperty<Color>(System.Windows.Media.Color.FromRgb(255,255,255))
                .AddTo(_disposables);
            Color.Subscribe(_ => SetColorCommand());

            MuteLedIsEnabled = new ReactiveProperty<bool>(GetMuteLedIsEnabled())
                .AddTo(_disposables);

            MuteLedIsEnabled.Subscribe(
                isEnabled => { Db1SyncUtilities.Write(SyncMessages.MakeSetMuteLedIsEnabledMessage(isEnabled)); })
                .AddTo(_disposables);

            SetRgbModeCommand = new ReactiveCommand()
                .WithSubscribe(SetRgbModeCommandExecute)
                .AddTo(_disposables);
            SetHsvCommand = new ReactiveCommand()
                .WithSubscribe(SetHsvCommandExecute)
                .AddTo(_disposables);
            IncreaseRgbModeSpeedCommand = new ReactiveCommand()
                .WithSubscribe(IncreaseRgbModeSpeedCommandExecute)
                .AddTo(_disposables);
            DecreaseRgbModeSpeedCommand = new ReactiveCommand()
                .WithSubscribe(DecreaseRgbModeSpeedCommandExecute)
                .AddTo(_disposables);
            IncreaseBrightnessCommand = new ReactiveCommand()
                .WithSubscribe(IncreaseBrightnessCommandExecute)
                .AddTo(_disposables);
            DecreaseBrightnessCommand = new ReactiveCommand()
                .WithSubscribe(DecreaseBrightnessCommandExecute)
                .AddTo(_disposables);
        }

        private bool GetMuteLedIsEnabled()
        {
            Db1SyncUtilities.WriteRead(SyncMessages.MakeGetMuteLedIsEnabledMessage(), out var returnBytes);

            if (returnBytes != null && returnBytes[1] > 0)
            {
                return true;
            }

            return false;
        }

        private void SetRgbModeCommandExecute()
        {
            Db1SyncUtilities.Write(SyncMessages.MakeRgbModeMessage(RgbModeUiToRgbMode()));
        }
        
        private void SetHsvCommandExecute()
        {
            // Set value (aka brightness) to max. Brightness button will adjust the value.
            Db1SyncUtilities.Write(SyncMessages.MakeHueMessage(Hue.Value, Saturation.Value, 255));
        }

        private void SetColorCommand()
        {
            System.Drawing.Color color = System.Drawing.Color.FromArgb(Color.Value.R, Color.Value.G, Color.Value.B);
            Hue.Value = (ushort)color.GetHue();
            Saturation.Value = (byte)(color.GetSaturation() * 255);
            Value.Value = (byte)(color.GetBrightness() * 510);
        }

        private void IncreaseRgbModeSpeedCommandExecute()
        {
            _rgbMode++;
            Db1SyncUtilities.Write(SyncMessages.MakeRgbModeMessage((SyncMessages.RgbMode)_rgbMode));
        }

        private void DecreaseRgbModeSpeedCommandExecute()
        {
            _rgbMode--;
            Db1SyncUtilities.Write(SyncMessages.MakeRgbModeMessage((SyncMessages.RgbMode)_rgbMode));
        }

        private void IncreaseBrightnessCommandExecute()
        {
            Db1SyncUtilities.Write(SyncMessages.MakeIncreaseBrightnessMessage());
        }

        private void DecreaseBrightnessCommandExecute()
        {
            Db1SyncUtilities.Write(SyncMessages.MakeDecreaseBrightnessMessage());
        }

        private SyncMessages.RgbMode RgbModeUiToRgbMode()
        {
            SyncMessages.RgbMode mode = SyncMessages.RgbMode.Invalid;

            switch (RgbMode.Value)
            {
                case RgbModeUi.Static:
                    _rgbMode = (byte)SyncMessages.RgbMode.Static;
                    mode = SyncMessages.RgbMode.Static;
                    break;
                case RgbModeUi.Breathing:
                    _rgbMode = (byte)SyncMessages.RgbMode.Breathing1;
                    mode = SyncMessages.RgbMode.Breathing1;
                    break;
                case RgbModeUi.RainbowSwirl:
                    _rgbMode = (byte)SyncMessages.RgbMode.RainbowSwirl1;
                    mode = SyncMessages.RgbMode.RainbowSwirl1;
                    break;
                case RgbModeUi.Snake:
                    _rgbMode = (byte)SyncMessages.RgbMode.Snake1;
                    mode = SyncMessages.RgbMode.Snake1;
                    break;
                case RgbModeUi.Knight:
                    _rgbMode = (byte)SyncMessages.RgbMode.Knight1;
                    mode = SyncMessages.RgbMode.Knight1;
                    break;
                case RgbModeUi.Xmas:
                    _rgbMode = (byte)SyncMessages.RgbMode.Xmas;
                    mode = SyncMessages.RgbMode.Xmas;
                    break;
                case RgbModeUi.StaticRainbow:
                    _rgbMode = (byte)SyncMessages.RgbMode.StaticRainbow1;
                    mode = SyncMessages.RgbMode.StaticRainbow1;
                    break;
                default:
                    break;
            }

            return mode;
        }

        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
