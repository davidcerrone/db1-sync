﻿using System;
using System.Windows.Media;
using Reactive.Bindings;

namespace Sync.ViewModels.Interfaces
{
    public enum RgbModeUi
    {
        Static,
        Breathing,
        RainbowSwirl,
        Snake,
        Knight,
        Xmas,
        StaticRainbow
    }

    public interface ILedConfigurationViewModel : IDisposable
    {
        ReactiveProperty<RgbModeUi> RgbMode { get; }

        ReactiveProperty<bool> MuteLedIsEnabled { get; }
        ReactiveProperty<ushort> Hue { get; }
        ReactiveProperty<byte> Saturation { get; }
        ReactiveProperty<byte> Value { get; }

        ReactiveProperty<Color> Color { get; set; }

        ReactiveCommand SetRgbModeCommand { get; }

        ReactiveCommand SetHsvCommand { get; }

        ReactiveCommand IncreaseRgbModeSpeedCommand { get; }

        ReactiveCommand DecreaseRgbModeSpeedCommand { get; }

        ReactiveCommand IncreaseBrightnessCommand { get; }

        ReactiveCommand DecreaseBrightnessCommand { get; }
    }
}