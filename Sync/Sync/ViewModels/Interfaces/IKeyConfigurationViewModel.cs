﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using Sync.Utilities;
using Reactive.Bindings;

namespace Sync.ViewModels.Interfaces
{
    public interface IKeyConfigurationViewModel : IDisposable
    {
        ReactiveCommand Key1Command { get; }
        ReactiveCommand Key2Command { get; }
        ReactiveCommand Key3Command { get; }
        ReactiveCommand Key4Command { get; }

        ReactiveProperty<Brush> Key1Background { get; }
        ReactiveProperty<Brush> Key2Background { get; }
        ReactiveProperty<Brush> Key3Background { get; }
        ReactiveProperty<Brush> Key4Background { get; }

        ReactiveProperty<string> EditKeyName { get; }
        ReactiveProperty<HidKeyboardKeypadUsage> EditKeyKeycode { get; }
        ReactiveCommand SetKeycode { get; }
        ReactiveCommand SetDefaultKeycodes { get; }
        ReactiveCommand SetMusicMode { get; }

        ReactiveProperty<bool> MacroGroupBoxEnabled { get; }

        ReactiveCommand SetMacro { get; }

        ReactiveProperty<MacroType> MacroType { get; }
        ReactiveProperty<byte> MacroDelayValue { get; }
        ReactiveProperty<HidKeyboardKeypadUsage> MacroKeycodeValue { get; }

        ReactiveCollection<MacroCommandObjectViewModel> ViewModelCollection { get; }
        ReactiveCommand AddMacroKeyCommand { get; }
        ReactiveCommand<MacroCommandObjectViewModel> RemoveCommand { get; }

        ReactiveProperty<string> MacroHints { get; }

        void SetSelectedKey(HidKeyboardKeypadUsage keycode);
        void SetSelectedKeyMacro(HidKeyboardKeypadUsage keycode);
    }
}
