﻿using System;
using System.Windows;
using Reactive.Bindings;

namespace Sync.ViewModels.Interfaces
{
    public interface IAboutViewModel : IDisposable
    {
        ReactiveProperty<string> DeviceDescription { get; }

        ReactiveCommand JumpToBootloaderCommand { get; }

        ReactiveCommand NullbitsWebsiteRequestedCommand { get; }

        ReactiveCommand CheckForUpdateCommand { get; }

        ReactiveCommand InstallUpdateCommand { get; }

        ReactiveProperty<string> UpdateStatus { get; }

        ReactiveProperty<Visibility> InstallButtonVisibility { get; }

        ReactiveProperty<string> SerialNumber { get; }
    }
}