﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Sync.Utilities;
using Sync.ViewModels.Interfaces;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using Reactive.Bindings.ObjectExtensions;
using Application = System.Windows.Application;

namespace Sync.ViewModels
{
    internal class KeyConfigurationViewModel : IKeyConfigurationViewModel
    {
        public ReactiveCommand Key1Command { get; private set; }
        public ReactiveCommand Key2Command { get; private set; }
        public ReactiveCommand Key3Command { get; private set; }
        public ReactiveCommand Key4Command { get; private set; }
        public ReactiveProperty<Brush> Key1Background { get; private set; }
        public ReactiveProperty<Brush> Key2Background { get; private set; }
        public ReactiveProperty<Brush> Key3Background { get; private set; }
        public ReactiveProperty<Brush> Key4Background { get; private set; }
        
        public ReactiveProperty<string> EditKeyName { get; private set; }
        public ReactiveProperty<HidKeyboardKeypadUsage> EditKeyKeycode { get; private set; }
        public ReactiveCommand SetKeycode { get; private set; }
        public ReactiveCommand SetDefaultKeycodes { get; private set; }
        public ReactiveCommand SetMusicMode { get; private set; }
        public ReactiveProperty<bool> MacroGroupBoxEnabled { get; private set; }
        public ReactiveCommand SetMacro { get; private set; }

        public ReactiveProperty<MacroType> MacroType { get; private set; }
        public ReactiveProperty<byte> MacroDelayValue { get; private set; }
        public ReactiveProperty<HidKeyboardKeypadUsage> MacroKeycodeValue { get; private set; }

        public ReactiveCollection<MacroCommandObjectViewModel> ViewModelCollection { get; private set; }
        public ReactiveCommand AddMacroKeyCommand { get; private set; }
        public ReactiveCommand<MacroCommandObjectViewModel> RemoveCommand { get; private set; }
        public ReactiveProperty<string> MacroHints { get; private set; }

        private Dictionary<SyncKey, KeyConfiguration> _keys;
        private SyncKey _currentEditingKey;
        private readonly CompositeDisposable _disposables;
        private const int MaxMacroLength = 14;

        #region Constructor

        public KeyConfigurationViewModel(CompositeDisposable disposables)
        {
            _disposables = disposables;

            CreateKeyDictionary();
            CreatePropertiesAndCommands();
        }

        #endregion

        #region Creation

        private void CreateKeyDictionary()
        {
            _keys = new Dictionary<SyncKey, KeyConfiguration>();

            _keys.Add(SyncKey.Key1, new KeyConfiguration(SyncKey.Key1, 0, 1, 0, (ushort)HidKeyboardKeypadUsage.KC_CHAT_MUTE_DEAFEN1, (ushort)HidKeyboardKeypadUsage.KC_AUDIO_MUTE));
            _keys.Add(SyncKey.Key2, new KeyConfiguration(SyncKey.Key2, 1, 1, 0, (ushort)HidKeyboardKeypadUsage.KC_CHAT_MUTE_DEAFEN2, 0xAC));
            _keys.Add(SyncKey.Key3, new KeyConfiguration(SyncKey.Key3, 1, 0, 0, (ushort)HidKeyboardKeypadUsage.KC_F13, 0xAE));
            _keys.Add(SyncKey.Key4, new KeyConfiguration(SyncKey.Key4, 0, 0, 0, (ushort)HidKeyboardKeypadUsage.KC_F14, 0xAB));
        }

        private void CreatePropertiesAndCommands()
        {
            Key1Command = new ReactiveCommand()
                .WithSubscribe(Key1CommandExecute)
                .AddTo(_disposables);

            Key2Command = new ReactiveCommand()
                .WithSubscribe(Key2CommandExecute)
                .AddTo(_disposables);

            Key3Command = new ReactiveCommand()
                .WithSubscribe(Key3CommandExecute)
                .AddTo(_disposables);

            Key4Command = new ReactiveCommand()
                .WithSubscribe(Key4CommandExecute)
                .AddTo(_disposables);

            Key1Background = new ReactiveProperty<Brush>(Application.Current.Resources["AccentBaseColorBrush"] as Brush)
                .AddTo(_disposables);

            Key2Background = new ReactiveProperty<Brush>(Brushes.Transparent)
                .AddTo(_disposables);

            Key3Background = new ReactiveProperty<Brush>(Brushes.Transparent)
                .AddTo(_disposables);

            Key4Background = new ReactiveProperty<Brush>(Brushes.Transparent)
                .AddTo(_disposables);
            
            _currentEditingKey = SyncKey.Key1;
            EditKeyName = new ReactiveProperty<string>("Key 1")
                .AddTo(_disposables);

            EditKeyKeycode = new ReactiveProperty<HidKeyboardKeypadUsage>(GetKeycode(SyncKey.Key1))
                .AddTo(_disposables);

            SetKeycode = new ReactiveCommand()
                .WithSubscribe(SetKeycodeExecute)
                .AddTo(_disposables);

            SetDefaultKeycodes = new ReactiveCommand()
                .WithSubscribe(SetDefaultKeycodesExecute)
                .AddTo(_disposables);

            SetMusicMode = new ReactiveCommand()
                .WithSubscribe(SetMusicModeExecute)
                .AddTo(_disposables);

            SetMacro = new ReactiveCommand()
                .WithSubscribe(SetMacroExecute)
                .AddTo(_disposables);

            MacroType = new ReactiveProperty<MacroType>(Utilities.MacroType.Type)
                .AddTo(_disposables);

            MacroDelayValue = new ReactiveProperty<byte>()
                .AddTo(_disposables);

            AddMacroKeyCommand = new ReactiveCommand()
                .AddTo(_disposables);

            MacroKeycodeValue = new ReactiveProperty<HidKeyboardKeypadUsage>(HidKeyboardKeypadUsage.KC_NO)
                .AddTo(_disposables);

            ViewModelCollection = AddMacroKeyCommand.Select(
                    x =>
                    {
                        if (MacroType.Value == Utilities.MacroType.Wait ||
                            MacroType.Value == Utilities.MacroType.Interval)
                        {
                            return new MacroCommandObjectViewModel(
                                new ReactiveProperty<MacroType>(MacroType.Value),
                                new ReactiveProperty<byte>(MacroDelayValue.Value),
                                new ReactiveProperty<string>($"{MacroDelayValue.Value} ms"));
                        }
                        return new MacroCommandObjectViewModel(
                            new ReactiveProperty<MacroType>(MacroType.Value),
                            new ReactiveProperty<byte>((byte)MacroKeycodeValue.Value),
                            new ReactiveProperty<string>(MacroKeycodeValue.Value.ToString("G")));
                    })
                .ToReactiveCollection()
                .AddTo(_disposables);

            ViewModelCollection.ObserveEveryValueChanged(x => x.Count)
                .Subscribe(
                    count =>
                    {
                        if (count > MaxMacroLength)
                        {
                            ViewModelCollection.RemoveAt(ViewModelCollection.Count - 1);
                        }
                    })
                .AddTo(_disposables);

            RemoveCommand = new ReactiveCommand<MacroCommandObjectViewModel>()
                .WithSubscribe(x => ViewModelCollection.Remove(x))
                .AddTo(_disposables);

            MacroHints = new ReactiveProperty<string>()
                .AddTo(_disposables);

            MacroGroupBoxEnabled = new ReactiveProperty<bool>(false)
                .AddTo(_disposables);
        }

        #endregion

        #region Command Execution

        private void SetKeycodeExecute()
        {
            if (_keys.TryGetValue(_currentEditingKey, out KeyConfiguration keyConfig))
            {
                Db1SyncUtilities.Write(SyncMessages.MakeSetDynamicKeymapMessage(keyConfig.Layer, keyConfig.Row, keyConfig.Column, (ushort)EditKeyKeycode.Value));
            }
        }

        private void SetDefaultKeycodesExecute()
        {
            foreach (var key in _keys.Values)
            {
                Db1SyncUtilities.Write(SyncMessages.MakeSetDynamicKeymapMessage(key.Layer, key.Row, key.Column, key.DefaultKey));
            }
        }

        private void SetMusicModeExecute()
        {
            foreach (var key in _keys.Values)
            {
                if (key.MusicKey != 0)
                {
                    Db1SyncUtilities.Write(SyncMessages.MakeSetDynamicKeymapMessage(key.Layer, key.Row, key.Column, key.MusicKey));
                }
            }
        }

        private void Key1CommandExecute()
        {
            _currentEditingKey = SyncKey.Key1;
            EditKeyName.Value = "Key 1";
            EditKeyKeycode.Value = GetKeycode(SyncKey.Key1);
            CheckIfMacroAndUpdateMacroList();
            SetHighlightedKey();
        }

        private void Key2CommandExecute()
        {
            _currentEditingKey = SyncKey.Key2;
            EditKeyName.Value = "Key 2";
            EditKeyKeycode.Value = GetKeycode(SyncKey.Key2);
            CheckIfMacroAndUpdateMacroList();
            SetHighlightedKey();
        }

        private void Key3CommandExecute()
        {
            _currentEditingKey = SyncKey.Key3;
            EditKeyName.Value = "Key 3";
            EditKeyKeycode.Value = GetKeycode(SyncKey.Key3);
            CheckIfMacroAndUpdateMacroList();
            SetHighlightedKey();
        }

        private void Key4CommandExecute()
        {
            _currentEditingKey = SyncKey.Key4;
            EditKeyName.Value = "Key 4";
            EditKeyKeycode.Value = GetKeycode(SyncKey.Key4);
            CheckIfMacroAndUpdateMacroList();
            SetHighlightedKey();
        }

        private void SetMacroExecute()
        {
            // Verify the macro before writing
            if (VerifyMacro())
            {
                byte[] buf = new byte[32];
                buf[0] = (byte)SyncMessages.MessageId.SetMacro;
                ArrayFormatter.WriteU16((ushort)EditKeyKeycode.Value, buf, 1);

                buf[3] = (byte)ViewModelCollection.Count; // length

                for (int i = 0; i < ViewModelCollection.Count; i++)
                {
                    buf[i * 2 + 4] = (byte)ViewModelCollection[i].MacroType.Value;
                    buf[i * 2 + 5] = ViewModelCollection[i].MacroValue.Value;
                }

                Db1SyncUtilities.Write(buf);
            }
        }

        private bool VerifyMacro()
        {
            if (EditKeyKeycode.Value == HidKeyboardKeypadUsage.KC_MACRO1)
            {
                return true;
            }
            else if (EditKeyKeycode.Value == HidKeyboardKeypadUsage.KC_MACRO2)
            {
                return true;
            }
            else if (EditKeyKeycode.Value == HidKeyboardKeypadUsage.KC_MACRO3)
            {
                return true;
            }
            else if (EditKeyKeycode.Value == HidKeyboardKeypadUsage.KC_MACRO4)
            {
                return true;
            }
            else
            {
                MacroHints.Value = "Invalid Keycode!\n\nSet Keycode to KC_MACRO*\nin single key section.";

                Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(x => MacroHints.Value = string.Empty);

                return false;
            }
        }

        private void SetHighlightedKey()
        {
            Key1Background.Value = Brushes.Transparent;
            Key2Background.Value = Brushes.Transparent;
            Key3Background.Value = Brushes.Transparent;
            Key4Background.Value = Brushes.Transparent;

            switch (_currentEditingKey)
            {
                case SyncKey.Key1:
                    Key1Background.Value = Application.Current.Resources["AccentBaseColorBrush"] as Brush;
                    break;
                case SyncKey.Key2:
                    Key2Background.Value = Application.Current.Resources["AccentBaseColorBrush"] as Brush;
                    break;
                case SyncKey.Key3:
                    Key3Background.Value = Application.Current.Resources["AccentBaseColorBrush"] as Brush;
                    break;
                case SyncKey.Key4:
                    Key4Background.Value = Application.Current.Resources["AccentBaseColorBrush"] as Brush;
                    break;
            }
        }

        #endregion

        #region Private Helpers

        private HidKeyboardKeypadUsage GetKeycode(SyncKey key)
        {
            ushort keycode = 0;

            if (_keys.TryGetValue(key, out KeyConfiguration keyConfig))
            {
                if (Db1SyncUtilities.WriteRead(SyncMessages.MakeGetDynamicKeymapMessage(keyConfig.Layer, keyConfig.Row, keyConfig.Column), out var readData))
                {
                    if (readData.Length > 0)
                    {
                        keycode = ArrayFormatter.ReadU16(readData, 4);
                    }
                }
            }

            return (HidKeyboardKeypadUsage)keycode;
        }

        private void CheckIfMacroAndUpdateMacroList()
        {
            bool isMacro = false;
            switch (EditKeyKeycode.Value)
            {
                case HidKeyboardKeypadUsage.KC_MACRO1:
                    isMacro = true;
                    break;
                case HidKeyboardKeypadUsage.KC_MACRO2:
                    isMacro = true;
                    break;
                case HidKeyboardKeypadUsage.KC_MACRO3:
                    isMacro = true;
                    break;
                case HidKeyboardKeypadUsage.KC_MACRO4:
                    isMacro = true;
                    break;
                default:
                    isMacro = false;
                    break;
            }

            if (isMacro)
            {
                MacroGroupBoxEnabled.Value = true;

                if (Db1SyncUtilities.WriteRead(
                    SyncMessages.MakeGetMacroMessage((ushort)EditKeyKeycode.Value),
                    out byte[] macroBytes,
                    32))
                {
                    try
                    {
                        ViewModelCollection.Clear();

                        var length = macroBytes[3];

                        // Parse the macro
                        for (int i = 4; i < (length * 2) + 4; i += 2)
                        {
                            if (macroBytes[i] == (int) Utilities.MacroType.Wait ||
                                macroBytes[i] == (int) Utilities.MacroType.Interval)
                            {
                                ViewModelCollection.Add(new MacroCommandObjectViewModel(
                                    new ReactiveProperty<MacroType>((MacroType)macroBytes[i]),
                                    new ReactiveProperty<byte>(macroBytes[i + 1]),
                                    new ReactiveProperty<string>($"{macroBytes[i + 1]} ms")));
                            }
                            else
                            {
                                ViewModelCollection.Add(new MacroCommandObjectViewModel(
                                    new ReactiveProperty<MacroType>((MacroType)macroBytes[i]),
                                    new ReactiveProperty<byte>(macroBytes[i + 1]),
                                    new ReactiveProperty<string>(((HidKeyboardKeypadUsage)macroBytes[i + 1]).ToString("G"))));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            else
            {
                MacroGroupBoxEnabled.Value = false;
            }
        }

        public void SetSelectedKey(HidKeyboardKeypadUsage keycode)
        {
            EditKeyKeycode.Value = keycode;
        }

        public void SetSelectedKeyMacro(HidKeyboardKeypadUsage keycode)
        {
            MacroKeycodeValue.Value = keycode;
        }

        #endregion

        #region IDisposable Implementation

        // To detect redundant calls
        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
